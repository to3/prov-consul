class prov_consul {

  ensure_packages('unzip')

  include ::consul

  ::prov_base::test::check {'consul_agent':
    port => 8500
  }
}
